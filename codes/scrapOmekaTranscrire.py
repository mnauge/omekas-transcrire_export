# -*- coding: utf-8 -*-
"""
Created on Mon May 17 09:14:07 2021

@author: Michael Nauge, Poitiers Univeristy
"""


import requests
from requests_html import HTML
from requests_html import HTMLSession

from bs4 import BeautifulSoup
import os

import pandas as pd
import glob


from datetime import datetime
from pathlib import Path

import shutil



class omekaTranscrireException(Exception):
    pass


def urlInterrogation(urlTarget):
    """
    Obtenir un objet BS contenant la page URL en entrée
    
    Parameters
    url : STR
        une url d'une page de document du site transcrire.
        
        exemple: https://transcrire.huma-num.fr/scripto/1/20/554
        
    Raises
    ------
    omekaTranscrireException
        Lève des exceptions si le serveur répond par un message d'erreur.
        
    Returns
    -------
    strTrans : bs4.BeautifulSoup
        objet BeautifulSoup contenant la page web
        
    
    """
    # Interrogation de la page URL
    try:
        
        #response = requests.get(urlTarget)
        session = HTMLSession()
        response = session.get(urlTarget)
 
    except requests.exceptions.RequestException as e:
        # Problème pour le protocole de requêtage
        # Potentiellement dangereux
        # A éviter
        print(e)
        response = requests.get(urlTarget, verify = False)        
    
    # Statut de la réponse:
    if response.status_code != 200:
        raise omekaTranscrireException
    else:
        return BeautifulSoup(response.text, 'html.parser')




def omkTransPage2StrTrans(urlTarget, withMarkup=False):
    """
    Obtenir une string contenant la transcription contenu
    dans la page web ciblée
    

    Parameters
    url : STR
        une url d'une page de document du site transcrire.
        
        exemple: https://transcrire.huma-num.fr/scripto/1/20/554
        
    withMarkup : BOOL
        False: ne conserve que le contenu sortie des balises P
        True : conserve tout le balisage html
        

    Raises
    ------
    omekaTranscrireException
        Lève des exceptions si le serveur répond par un message d'erreur.
        
    Returns
    -------
    strTrans : STR
        une str contenant la transcription obtenue
        
    
    """
    
    
    strTrans = ""
    
    #response = requests.get(urlTarget) 
    session = HTMLSession()
    response = session.get(urlTarget)
    
    soup = BeautifulSoup(response.text,'html.parser')
    souplettesTrans = soup.find('div',{"data-type":"transcription"})
    
    if withMarkup==False:
        listP = souplettesTrans.findAll('p')
        
        for p in listP :
            strTrans+=p.text
    else:
        strTrans = souplettesTrans.contents
    
    

    return strTrans


def omkTransBS2StrTrans(soup, markuped):
    """
    Obtenir une string contenant la transcription contenu
    dans la page web ciblée
    

    Parameters
    soup : bs4.BeautifulSoup
        objet BeautifulSoup contenant la page web

    Returns
    -------
    strTrans : STR
        une str contenant la transcription obtenue
    
    """
    # Nettoyage de l'objet pour récupérer la transcription
    soup = soup.find('body')
    soup = soup.find('aside')
    soup = soup.find('div', {"data-type" : "transcription"})
    paragraphe = soup.findAll('p')

    stringToReturn = ""
    
    if markuped==False:

        for par in paragraphe:
            stringToReturn += par.text
    else:
        for content in soup.contents:
            stringToReturn += str(content)

        

    return stringToReturn

def omkTransPage2StrJpg(urlTarget):
    """
    Obtenir le nom identifier du fichier JPG transcrit de la page cible
    

    Parameters
    ----------
    urlTarget : STR
        une url d'une page de document du site transcrire.
        
        exemple: https://transcrire.huma-num.fr/scripto/1/20/554.

    Returns
    -------
    jpgFileIdentifier : STR

    """
    
    
    strJpgId = ""
    
    #response = requests.get(urlTarget)   
    session = HTMLSession()
    response = session.get(urlTarget)
    
    soup = BeautifulSoup(response.text,'html.parser')
    souplettesBody = soup.find('body')
    soupletteAside = souplettesBody.find('aside')

    finded = soupletteAside.findAll('div',{"class":"property"})

    for v in finded:
        vH4 = v.find('h4').text.strip()
        if "Identifiant" in vH4:
     
            soupletteV = v.find('div',{"class":"value"})
            vJPG = soupletteV.text.strip()
            strJpgId = vJPG

    return strJpgId


def omkTransBS2StrJpg(soup):
    """
    Obtenir le nom identifier du fichier JPG transcrit de la page cible
    

    Parameters
    ----------
    soup : bs4.BeautifulSoup
        objet BeautifulSoup contenant la page web

    Returns
    -------
    jpgFileIdentifier : STR

    """
    # Préparation du string
    soup = soup.find('body')
    soup = soup.find('aside')
    soup = soup.findAll('div',{"class":"property"})

    for v in soup:
        vdt = v.find('dt').text.strip()
        if vdt.lower() == "identifiant":
            identifiant = v.find('span', {'class': 'value-content'})

    return identifiant.text.strip()    

def omkTransPage2Txt(urlTarget, pathDirTxt):
    """
    Générer un fichier TXT (utf-8) contenant la transcription contenu
    dans la page web ciblée. 
    Le nom du fichier TXT doit être celui de l'image JPG.
    
    Cette fonction utilise 
    la fonction omkTransPage2StrTrans(urlTarget) pour obtenir la transcription.
    la fonction omkTransPage2StrJpg(urlTarget) pour obtenir le nom du fichier JPG
    transcrit
    
    

    Parameters
    urlTarget : STR
        une url d'une page de document du site transcrire.
        
        exemple: https://transcrire.huma-num.fr/scripto/1/20/554
        
        
    pathDirTxt : STR
        le chemin du dossier pour recevoir le fichier TXT de sortie

    
    Raises
    ------
    omekaTranscrireException
        Lève des exceptions si le serveur répond par un message d'erreur.
           
    
    """
    
    
    strTxt = omkTransPage2StrTrans(urlTarget)
    filenameJpg = omkTransPage2StrJpg(urlTarget)
    filenametxt = filenameJpg.replace('.jpg','.txt')
    print(strTxt)
    print(filenameJpg)
    print(filenametxt)
       
    with open(pathDirTxt+filenametxt, 'w', encoding='utf-8') as txtOut:
        txtOut.write(strTxt)



def omkTransCarnet2listUrlPages(urlTargetCarnet):
    
    """
    Générer la liste contenant les liens vers les pages
    contenant des transcriptions pour le carnet précisé par son
    urlTarget.
    
    Parameters
    urlTarget : STR
        une url de carnet.
        
        exemple:  "https://transcrire.huma-num.fr/scripto/1/20/media"

    Raises
    ------
    omekaTranscrireException
        Lève des exceptions si le serveur répond par un message d'erreur.
           
    
    """
    

    
    # a cause de la pagination il va falloir faire plusieurs requetes.
    
    #https://transcrire.huma-num.fr/scripto/1/20/media?sort_by=position&sort_order=asc&page=1
    
    # code de Julie Courgibet réalisé pendant le https://gitlab.huma-num.fr/mnauge/cnam-athon-transcrire
    # Merci !
    
    # Interrogation de la page URL
    soup = urlInterrogation(urlTargetCarnet)
    start_url = "https://transcrire.huma-num.fr"

    # Initialisation de la liste URLs du projet
    liste_URL = []

    # Récupération des nombres de pages pour accéder à tous les documents du carnet
    # Vérifier structure de page si 1 seule page de résultat
    # Exemple 1 seule page de résultats :
    # https://transcrire.huma-num.fr/scripto/8/101/media
    soup = soup.find('body')
    soup = soup.find('div', {"id": "content"})
    soupPage = soup.find('div', {"class": "wrapper__header"})
    soupPage = soupPage.find("nav").find('span', {"class": "arrow-pagination__text"})
    soupPage = int(soupPage.text.split(" sur ")[1])

    # Recherche des URLs contenues dans la page
    soupURL = soup.find('div', {"class": "wrapper__main wrapper__main_small original-title-hidden"})
    soupURL = soupURL.find('main', {"class":"wrapper__content"})
    soupURL = soupURL.find('ul', {"class" : "thumbnail-links__list thumbnail-links_3__list"})
    soupURL = soupURL.findAll("li")
    for url in soupURL:
        try:
            url = url.find("a").get('href')
            liste_URL.append(start_url + url)
        except:
            pass

    if soupPage == 1 :
        pass
    else:
        for pageCarnet in range(2, int(soupPage+1)):
            url_next = urlTargetCarnet + "?sort_by=position&sort_order=asc&page=" + str(pageCarnet)
            soupPage = urlInterrogation(url_next)
            
            print("request",url_next)


            # Traitement de la page BS
            soupPage = soupPage.find('body')
            soupPage = soupPage.find('div', {"id": "content"})
            # Recherche des URLs contenues dans la page
            soupURL = soupPage.find('div', {"class": "wrapper__main wrapper__main_small original-title-hidden"})
            soupURL = soupURL.find('main', {"class":"wrapper__content"})
            soupURL = soupURL.find('ul', {"class" : "thumbnail-links__list thumbnail-links_3__list"})
            soupURL = soupURL.findAll("li")
            for url in soupURL:
                try:
                    url = url.find("a").get('href')
                    liste_URL.append(start_url + url)
                except:
                    pass

    return liste_URL
        
def omkTransCarnetGetTitle(urlTargetCarnet):
    # Interrogation de la page URL
    soup = urlInterrogation(urlTargetCarnet)
    
    h1 = soup.find('h1')
    
    strH1 = h1.text

    
    return strH1
    
    

def omkTransCarnet2Txt(urlTargetCarnet, markuped, pathDirTxt):
    """
    Générer tous les fichiers TXT contenant toutes les transcriptions d'un carnet
    
    Cette fonction utilise 
    la fonction omkTransCarnet2listUrlPages(urlTargetCarnet) pour obtenir la liste des url de page à visiter
    la fonction omkTransPage2Txt(urlTargetPage) pour produire le TXT d'une page cible
    
    

    Parameters
    urlTargetCarnet : STR
        une url de carnet du site transcrire.
        
        exemple: https://transcrire.huma-num.fr/scripto/8/122/media
        
        
    pathDirTxt : STR
        le chemin du dossier pour recevoir les fichiers TXT de sortie

    
    Raises
    ------
    omekaTranscrireException
        Lève des exceptions si le serveur répond par un message d'erreur.
           
    
    """
    
    
    if os.path.exists(pathDirTxt):
        pass
    else:
        os.makedirs(pathDirTxt)

    list_URL = omkTransCarnet2listUrlPages(urlTargetCarnet)
    nbUrl = str(len(list_URL))
    print("nb. url to parse :", nbUrl)
    
    for count,urlTarget in enumerate(list_URL,start=1):
        print(count,"/",nbUrl," : ",urlTarget)
        
        pageSoup = urlInterrogation(urlTarget)
        transcription = omkTransBS2StrTrans(pageSoup, markuped)
        nomFichier = omkTransBS2StrJpg(pageSoup)
        nomFichier = nomFichier.replace('.jpg','')
        nomFichier = nomFichier.replace('.JPG','')
        
        validName = ""
        for c in nomFichier:
            if c.isalnum():
                validName+=c
            else:
                validName+="_"
            
        
        nomFichier = validName

        if pathDirTxt.endswith("/") or pathDirTxt.endswith("\\"):
            pathFile = pathDirTxt + nomFichier + ".txt"
        else:
            pathFile = pathDirTxt + "/" + nomFichier + ".txt"
        
        with open(pathFile, 'w', encoding='utf-8') as f:
            f.write(transcription)
            
            print(pathFile, "[saved]")

    
    
def dirTxt2df(pathDirTxt):
    
    
    dfTrans = pd.DataFrame(columns=['idFile','transcription'])
    
    print(pathDirTxt)
    listTxt = glob.glob(pathDirTxt+"*.txt")
    print(listTxt)
    
    for pthTxt in listTxt:
        
        with open(pthTxt,encoding='utf-8') as trans:
            strTrans = trans.readlines()
            strTrans = ''.join(strTrans)
            print(strTrans)
            identifier = os.path.basename(pthTxt).replace('.txt','.jpg')
            print(identifier)
            dicRow = {'idFile':[identifier], 'transcription':[strTrans]}
            
            #dfTrans = dfTrans.append(dicRow, ignore_index=True)
            
            dfRowCurrent = pd.DataFrame(data=dicRow)
            # ajouter cette matrice à la suite de l'autre (vide dans notre cas)
            dfTrans = pd.concat([dfTrans, dfRowCurrent], ignore_index=True)
        #filename = 
    return dfTrans


def getDictAuthorTranscript(idPage):
    
    """
    Obtenir une liste de dictionnaire contenant date, auteur, état contenant 
    l'historique de transcription. Passer par omeka S ou le wiki.
    
    Parameters
    idPage : STR
        un id de page
        
        exemple:  "8/122/11398"
        urlTarget : "https://transcrire.huma-num.fr/scripto/8/122/11398/revision" 
        ou
        urlTarget : "https://transcrire.huma-num.fr/MEDIAWIKI/index.php?title=8:122:11398&action=history"

        

    Returns
    -------
    listDictHist : LIST[DICT{date,author,state}]
    """
    listDic = []
    
    try:
        # Interrogation de la page URL
        urlTarget = "https://transcrire.huma-num.fr/scripto/" + idPage + "/revision" 
        soup = urlInterrogation(urlTarget)
        soup = soup.find("body")
        soup = soup.find("div", {"id": "content"})
        soup = soup.find("div", {"class": "wrapper__main"})
        soup = soup.find("main", {"class": "wrapper__content"})
        soup = soup.find("div", {"class": "table_wrapper"})
        soupTable = soup.find("table")
        soupTable = soupTable.find("tbody")
        # Première ligne de la table suffit
        # Faire FindAll si toutes les lignes nécessaires
        #soupTableLine = soupTable.find("tr")
        
        soupLines = soupTable.findAll("tr")
        #print(soupLines)
    
        for soupTrLine in soupLines:
    
            
            # Recherche des champs td et a pour date et auteur
            champs = soupTrLine.findAll("td")
            
    
            dic_to_return = {}
            # Champs toujours aux mêmes positions
            for num_champ, champ in enumerate(champs):
                # Date
                if num_champ == 2:
                    dic_to_return["date"] = champ.find("a").text
                    if champ.find("p"):
                        dic_to_return["etat"] = champ.find("p").text
                    else:
                        dic_to_return["etat"] = "en cours"
                # Auteur
                if num_champ == 3:
                    dic_to_return["auteur"] = champ.find("a").text
                
            listDic.append(dic_to_return)
        
    except:
        print("no history for",urlTarget)
    
    return listDic


def omkTransCarnet2dfHistContrib(urlTargetCarnet, ):
    
    print("start history contribution extraction")
    
    #creation de la dataframe resultat
    dfAuth = pd.DataFrame(columns=['identifier','date','etat','auteur','idPage'])
    
    
    
    #obtenir la liste des urls de toutes les pages du carnet
    list_URL = omkTransCarnet2listUrlPages(urlTargetCarnet)
    nbUrl = str(len(list_URL))
    print("nb. url to parse for history contribution :", nbUrl)
    
    for count, urlTarget in enumerate(list_URL,start=1):
        print(count,"/",nbUrl," : ",urlTarget)
        #obtenir identifiant page jpg
        pageSoup = urlInterrogation(urlTarget)
        nomFichier = omkTransBS2StrJpg(pageSoup)
        
        
        # aller requete l'historique
        idPage = urlTarget.replace('https://transcrire.huma-num.fr/scripto/', '')
        listDicAutPage = getDictAuthorTranscript(idPage)
        
        for dicAuth in listDicAutPage:
            dicAuth['identifier'] = [nomFichier]
            dicAuth['idPage'] = [idPage]
            dicAuth['date'] = [dicAuth['date']]
            dicAuth['auteur'] = [dicAuth['auteur']]
            dicAuth['etat'] = [dicAuth['etat']]


            #dfAuth = dfAuth.append(dicAuth, ignore_index=True)
            
            dfRowCurrent = pd.DataFrame(data=dicAuth)
            # ajouter cette matrice à la suite de l'autre (vide dans notre cas)
            dfAuth = pd.concat([dfAuth, dfRowCurrent], ignore_index=True)
        
    
    print(dfAuth)
    print("end history contribution extraction")
    return dfAuth



def omkTransCarnet2dfMetas(urlTargetCarnet):
    
    """
    Obtenir une dataframe contenant le plus possible de métadonnées d'un carnet
    
    
    

    Parameters
    urlTargetCarnet : STR
        une url de carnet.
        exemple:  "https://transcrire.huma-num.fr/scripto/8/122/media"

        

    Returns
    -------
    dfMetas : Dataframe 
        Cette dataframe doit contenir le plus possible de métadonnées.
        Pour commencer, il faut les colonnes minimales suivantes : url, idFile
        
    """
    pass
    
   
def extractCarnet(urlTargetCarnet, markuped):
    print("start extraction carnet",urlTargetCarnet)
    carnetName = omkTransCarnetGetTitle(urlTargetCarnet)
    
    #remplacer les caractères qui ne vont pas pour un nom de dossier.
    validName = ""
    for c in carnetName:
        if c.isalnum():
            validName+=c
        else:
            validName+="_"
    
    now = datetime.now() # current date and time
    date_time = now.strftime("%m-%d-%Y_%H-%M-%S")
	
    validNameDated = validName+"_"+date_time
    pathRootDir = "./../datas/"+validNameDated+"/"
    print("make directory","./../datas/"+validNameDated)
    
    Path(pathRootDir).mkdir(parents=True, exist_ok=True)
    
    omkTransCarnet2Txt(urlTargetCarnet, markuped, pathRootDir)

    dfTrans = dirTxt2df(pathRootDir)
    dfTrans = dfTrans.sort_values(by=['idFile'])
    dfTrans.to_excel(pathRootDir+"transcriptionsMerged.xlsx", index=False)

    dfContrib = omkTransCarnet2dfHistContrib(urlTargetCarnet)
    dfContrib = dfContrib.sort_values(by=['identifier'])
    dfContrib.to_excel(pathRootDir+"contributorHistory.xlsx", index=False)
    
    shutil.make_archive('./../datas/'+validNameDated, 'zip', pathRootDir)
    
    print("stop extraction carnet",urlTargetCarnet)
    
    pathZip = './../datas/'+validNameDated+".zip"
    return pathZip

    
    
    