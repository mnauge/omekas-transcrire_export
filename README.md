# OmekaS-Transcrire_Export
*Michael Nauge, Laboratoire FoReLLIS/MIMMOC, Université de Poitiers (2021)*

Outils permettant l'export des transcriptions du site [Transcrire](https://transcrire.huma-num.fr/) (version OmekaS).

## Objectifs

Lorsqu'un document est entièrement transcrit et approuvé, il est important de pouvoir disposer facilement des transcriptions de chaque page du document et de connaître l'historique des contributions de chaque page.

Cependant, en l'état actuel des developpements du site Transcrire, il n'y a pas de procédure d'export permettant la récupérations des données attendues.
C'est pour répondre à ces attentes qu'un developpement en Python a été réalisé ([cf codes](./codes/)) et dont une interface graphique minimaliste a été réalisée en utilisant un notebook [jupyter](https://jupyter.org/) "déployé" en application web avec le système [voilà](https://voila.readthedocs.io/en/stable/index.html).

Pour lancer l'interface d'extraction de données veuillez utiliser le bouton ci-dessous :

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.huma-num.fr%2Fmnauge%2Fomekas-transcrire_export/HEAD?urlpath=voila%2Frender%2Fnotebook%2FextractCarnet.ipynb%2F)


## En cas de problème

Si l'application distante MyBinder rencontre un problème,
il est possible de lancer l'interface d'extraction sur son ordinateur personnel.
Pour cela :
1. Téléchargez sur votre bureau et décompresser l'[archive](https://gitlab.huma-num.fr/mnauge/omekas-transcrire_export/-/archive/master/omekas-transcrire_export-master.zip)
1. Installez l'environnement de datascientist [Anaconda](https://www.anaconda.com/products/distribution)
1. Lancez le logiciel Jupyter (inclus dans Anaconda), ce qui lancera également votre navigateur internet
1. Depuis Jupyter, naviguez dans vos dossiers pour ouvrir l'archive téléchargée et lancer le notebook extractCarnetLocal.ipynb. Exemple : Desktop\omekas-transcrire_export\notebook\extractCarnetLocal.ipynb
1. Une fois le fichier **extractCarnetLocal.ipynb** ouvert dans Jupyter, utiliser le menu>Cell>Run All
1. Utilisez la zone de saisie et le bouton fraîchement affichée en bas du notebook pour démarrer l'extraction 

## Remerciements

Les codes ont été réalisées lors d'un [hackathon](https://gitlab.huma-num.fr/mnauge/cnam-athon-transcrire) dans le cadre de la formation CNAM [Ingénieur Big Data et Intelligence Artificielle](https://www.cnam-nouvelle-aquitaine.fr/alternance/big-data/ingenieur-big-data-et-intelligence-artificielle.html) en convention avec l'Université de Poitiers.

Je tiens à remercier tous les étudiants de cette promotion 2021 pour leurs participations actives et un remerciement tout particulier pour Julie, Nicolas et Julien pour leurs initiatives et la qualité de leurs propositions de codes.

## Algorithme

### Entrée
- Url d'un document à exporter
   - exemple : https://transcrire.huma-num.fr/scripto/8/108/media
   
   
### Sorties
Un dossier .ZIP nommé grâce au "title H1" du document et de la date-heure du lancement de l'export. Ce dossier contient :
- un fichier TXT par page du document. Chaque fichier TXT est nommé en utilisant l'identifiant présent dans la notice de chaque page.
    - exemple : Identifiant= FRAD86_16J3_38_138_10621.jpg pour la page https://transcrire.huma-num.fr/scripto/8/108/10582 va produire un fichier TXT nommé FRAD86_16J3_38_138_10621.txt
    
- un tableur XLSX nommé transcriptionsMerged.xlsx agrégeant l'intégralité des transcriptions. Ce tableur contient deux colonnes : 
    - idFile : l'identifiant de page
    - transcription : la transcription de la page identifiée
    
- un tableur XLSX nommé contributorHistory.xlsx agrégent l'historique des interactions sur la transcription de chaque page. Ce tableur contient quatre colonnes :
    - identifier : l'identifiant de page
    - date : date d'interaction sur la transcripition de la page
    - etat : valeurs possibles (en cours, approuvé)
    - auteur : le pseudo Transcrire de l'auteur de l'intéraction
    - idPage : l'url de la page
    
    
### Étapes

```
0. Création du dossier d'export

titreDocument = Requetes+Parsing pour obtenir le titre du document

date_heure du lancement de l'export

Créer un dossier nommé par titreDocument_date_heure

1. Extraction des transcriptions

listUrlPage = Requetes+Parsing pour obtenir la liste des URLs de toutes les pages d'un carnet

Pour chaque URL de listUrlPage
    idPage = Requete+Parsing pour extraire l'identifiant de la page dans la notice de la page
    
    strTranscription = Requete+Parsing pour extraire la transcription de la page
    
    Sauvegarder un fichier TXT nommé par l'idPage contenant la strTranscription (encodage utf-8) dans le dossier titreDocument_date_heure

Fusionner tous les fichiers TXT dans le tableur transcriptionsMerged.xlsx dans le dossier titreDocument_date_heure


2. Extraction des historiques d'interactions

listUrlPageHist = Requetes+Parsing pour obtenir la liste des URLs d'historique de toutes les pages d'un carnet

Pour chaque URL de listUrlPageHist
    
    listDicHistory = Requete+Parsing pour extraire l'historique d'interaction de la page

Fusionner toutes les listes de DicHistory dans le tableur contributorHistory.xlsx dans le dossier titreDocument_date_heure


3. Compresser en ZIP le dossier titreDocument_date_heure
    

```
    






